import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppConstants } from '../app.constants';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Injectable({
    providedIn: 'root'
})
export class LocateService {
    private allCities = `${AppConstants.API_ENDPOINT}${AppConstants.API.CITIES}`;

    constructor(private http: HttpClient) { }

    getAllStates(): Observable<any> {
        return this.http.get('./assets/mockData/state.json');
    }

    getStateDetails(params) {
        return this.http.get<any>(`${AppConstants.API_ENDPOINT}${AppConstants.API.CITIES}?${params}`);
    }
}