import { Component, ChangeDetectorRef } from '@angular/core';
import { LocateService } from './state-location.service';
import { Subscription } from 'rxjs';
import { GeocodeService } from './geo-code.service';
import { Location } from './state-location-model';

@Component({
  selector: 'app-state-location',
  templateUrl: './state-location.component.html',
  styleUrls: ['./state-location.component.scss']
})
export class StateLocationComponent {
  subscriptions: Subscription = new Subscription();
  allStates: any;
  allCities: any[];
  address = '';
  location: Location;
  loading: boolean;
  showCities = false;
  citySelected = false;
  searchDistrict = '';
  userSelectedState = '';
  userSelectedDistrict = '';
  userSelectedCity = '';
  showAlertMessage = false;
  completeData: any[];

  constructor(private _LocateService: LocateService,
    private _GeocodeService: GeocodeService,
    private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.fetchAllStates(); // fetch all state from mockjson
  }

  // fetching all the states from mockjson
  fetchAllStates() {
    this.subscriptions.add(this._LocateService.getAllStates()
      .subscribe(locationData => {
        this.completeData = locationData.states;  // initialize complete data from mockjson
        const statesArr = [];
        locationData.states.forEach(individualStates => {
          statesArr.push(individualStates.state);
        });
        // removing duplicates
        this.allStates = Array.from(new Set(statesArr)).sort();
        this.processSelection('Karnataka')
      }));
  }

  // on select state from dropdown
  selectedState(selectedState) {
    this.reset();
    this.processSelection(selectedState);
  }
  
  
  /**
   * get all district based on state name
   * @param selection data is state name
   */
  processSelection(selection) {
    const selectedSateData = this.completeData.filter(allStates => allStates.state === selection)[0];
    const statesArr = [];
    selectedSateData.districts.forEach(individualCities => {
      statesArr.push(individualCities);
    });
    this.allCities = statesArr.sort();
    if (this.allCities.length > 0) {
      this.showCities = true;
          if (!this.userSelectedCity) {
            this.selectedCity('Bangalore');
          }
        } else {
          this.showCities = false;
          this.showAlert();
        }
      }

    // on select city in left column
    selectedCity(city) {
    this.userSelectedCity = city;
    this.address = city;
    this.citySelected = true;
    this.showLocation();
  }

  showLocation() {
    this.addressToCoordinates();
  }

  // google maps api based on place names
  addressToCoordinates() {
    this.loading = true;
    this.subscriptions.add(this._GeocodeService.geocodeAddress(this.address)
      .subscribe((location: Location) => {
        this.location = location;
        this.loading = false;
        this.ref.detectChanges();
      }
      ));
  }

  // reseting all the fields
  reset() {
    this.searchDistrict = '';
    this.userSelectedState = '';
    this.userSelectedDistrict = '';
    this.userSelectedCity = '';
    this.citySelected = false;
  }

  showAlert() {
    this.showAlertMessage = true;
    setTimeout(() => {
      this.showAlertMessage = false;
    }, 3000);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
