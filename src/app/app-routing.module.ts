import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StateLocationComponent } from './state-location/state-location.component';



const routes: Routes = [
    { path: '', redirectTo: 'locate', pathMatch: 'full' },
    { path: 'locate', component: StateLocationComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
